A MySQL (MariaDB) RestExpress Server + Hibernate + Kafka.
============================
A MySQL (MariaDB) RestExpress Server that POSTs to a Kafka queue for a consumer to write to the db. 

Database Setup
--------------

Hibernate framework will be used to connect/use mysql to both RestExpress and Kafka frameworks. 

Make sure you have a mysql database running and create or edit hibernate.cfg.xml file in 
/src/main/resources/ and write your local database credentials, driver, connector, 
and hibernate dialect info onto this configuration file.

Make sure you create a table called JOB. use the following sql:

    create table JOB( id INT NOT NULL auto_increment, title VARCHAR(20) default NULL, PRIMARY KEY (ID) );

Build
-----

To create a 'fat' runnable jar file with dependecies:

	mvn clean compile assembly:single

To run the jar file (Main RestExpressServer) created via package

	java -jar target/{project-name}.jar [environment]


Configuration
-------------

By default, the 'mvn package' goal will create a fat jar file including the configuration files in src/main/resources.
These are loaded from the classpath at runtime. However, to override the values embedded in the jar file, simply create
a new configuration file on the classpath for the desired environment. For example, './config/dev/environment.properties'
and any settings in that file will get added to, or override settings embedded in the jar file.


GET request
-----------

Once you run the RestExpress Server on 0.0.0.0:8081 you will be able to request a Job collection. 
The route set in the Routes.java file for the GET request is 0.0.0.0:8081/test/jobs.json.
Either using a browser or postman you will be able to get an index of jobs stored in the JOB table 
within the mysql db. 

Note** The table is empty initially and will be populated using the 
TestMain.java program

Kafka and ZooKeeper Servers
-----------

Before moving to the POST request, we need to make sure the ZooKeeper and Kafka Servers are running.

To run the servers cd into the kafka folder and run the sh scripts:

    for ZooKeper Server run:
        kafka_2.11-1.1.0/bin/zookeeper-server-start.sh config/zookeeper.properties
    
    for Kafka Server run:
        kafka_2.11-1.1.0/bin/kafka-server-start.sh config/server.properties
    
Also you need to create a topic where your POST requests will be send as producers. 

    run the following to create the topic "my-example-topic":
    
        kafka_2.11-1.1.0/bin/kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic my-example-topic
    
    
Finally, you also need to start the consumer kafka sever by running the following class after compiling the jar file with dependecies:
    java -cp ./target/{project-name}.jar com.risksense.assessment.job.KafkaConsumerExample

POST request
-----------

Once you have the four things needed to configure kafka you may begin doing some POST requests. 
The route set in the Routes.java file for the POST request is also 0.0.0.0:8081/test/jobs.json.
Using postman or another tool to do a POST request you will be able to create a Job entry 
in the JOB table of the mysql db. In the raw body of your request you need to provide the attributes 
of a Job entry. The "raw body" should be a JSON formatted string as shown below. 

    {
    "title":"Software Engineer", 
    "skills":"math",
    "posted_date":"06012018",
    "availability":"part-time",
    "job_type":"programmer",
    "experience_level":"6 years",
    "country":"USA",
    "languages":"English",
    "pay_rate_max":200,
    "pay_rate_min":51,
    "extra":180
    }

You can include other attribute key,value pairs like the "extra":180 but since they are not part of the 
POJO Job class then those attributes won't affect the POST request. You have to provide valid JSON or 
you will get 400 errors. 


Testing POST 100k job requests 
------------------------------

After having the zookeper, kafka, and restExpress servers running in  different terminals, you can run the following TestMain.class 
file to invoke 100k POST requests to restExpress. TestMain will excute its own kafka consumer so the 
previus kakfa consumer server can be stopped but you may leave it running and you will have two consumers running instead of one. 

To Run: 
    java -cp ./target/{project-name}.jar com.risksense.assessment.TestMain

Normally, it takes about ~30 minutes to put the 100k jobs in the kafka queue and about ~73 seconds to write the jobs into the mysql db