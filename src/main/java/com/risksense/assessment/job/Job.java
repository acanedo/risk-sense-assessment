package com.risksense.assessment.job;


public class Job {
	   private int id;
	   private String title, skills, posted_date, availability, job_type, experience_level, country, languages;
	   private int pay_rate_min, pay_rate_max;  
	   
	   

	   public Job() {
	   }
	   public Job(String title, String skills, String posted_date, String availability, String job_type, String experience_level, String country, String languages, int pay_rate_min, int pay_rate_max ) {
	      this.title = title;
	      this.skills = skills ;
	      this.posted_date = posted_date;
	      this.availability = availability;
	      this.job_type = job_type;
	      this.experience_level = experience_level;
	      this.country = country;
	      this.languages = languages;
	      this.pay_rate_max = pay_rate_max;
		  this.pay_rate_min = pay_rate_min;
	   }
	   
	   public int getId() {
	      return id;
	   }
	   
	   public void setId( int id ) {
	      this.id = id;
	   }
	   
	   public String getTitle() {
	      return title;
	   }
	   
	   public void setTitle( String title ) {
	      this.title = title;
	   }
	   public String getSkills() {
		  return skills;
	   }
		   
	   public void setSkills( String skills ) {
		  this.skills = skills;
	   }   
	   public String getPostedDate() {
		  return posted_date;
	   }
		   
	   public void setPostedDate( String posted_date ) {
		  this.posted_date = posted_date;
	   } 
	   public String getAvailability() {
		  return availability;
	   }
		   
	   public void setAvailability( String availability ) {
		  this.availability = availability;
	   } 
	   public String getJobType() {
		  return job_type;
	   }
		   
	   public void setJobType( String job_type ) {
		  this.job_type = job_type;
	   }
	   public String getExperienceLevel() {
			  return experience_level;
	   }
			   
	   public void setExperienceLevel( String experience_level ) {
		   this.experience_level = experience_level;
	   }
	   
	      	  
		public String getCountry() {
			return country;
		}
			   
		public void setCountry( String country ) {
			this.country = country;
		}
		public String getLanguages() {
			return languages;
		}
			   
		public void setLanguages( String languages ) {
			this.languages = languages;
		}
		public int getPayRateMax() {
			return pay_rate_max;
		}
			   
		public void setPayRateMax( int pay_rate_max) {
			this.pay_rate_max = pay_rate_max;
		}
		public int getPayRateMin() {
			return pay_rate_min;
		}
			   
		public void setPayRateMin( int pay_rate_min ) {
			this.pay_rate_min = pay_rate_min;
		}
		
	 
	}