package com.risksense.assessment.job;

import java.util.Collections;
import java.util.List;

import org.restexpress.Request;
import org.restexpress.Response;



import java.util.Date;
import java.util.Iterator;

import org.hibernate.HibernateException; 
import org.hibernate.Session; 
import org.hibernate.Transaction;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;


public class JobController
{
	private static SessionFactory factory; 
	
	public JobController()
	{
		super();
	}

	public Object create(Request request, Response response) throws Exception
	{
		//TODO: Your 'POST' logic here...
		
		//mapper to convert object to json string and send to producer
		ObjectMapper mapper = new ObjectMapper();
			
		System.out.print("POST REQUEST\n"); 
		Job job = request.getBodyAs(Job.class, "error for job details");
		
		
		//convert POJO to JSON
		try {
			String jsonValues = mapper.writeValueAsString(job);
			System.out.print(jsonValues); 
			
			KafkaProducerExample.runProducer(jsonValues);
		}
		catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		  
	      
	      return job;
	}

	public List read(Request request, Response response) throws Exception
	{
		
		//TODO: Your 'GET' logic here...
		
		try {
	         factory = new Configuration().configure().buildSessionFactory();
	      } catch (Throwable ex) { 
	    	  //ex.printStackTrace();
	         System.err.println("Failed to create sessionFactory object." + ex);
	         throw new ExceptionInInitializerError(ex); 
	      }
		
		
		Session session = factory.openSession();
	    Transaction tx = null;
	    
	    try {
	    	System.out.print("Reached Controller\n"); 
	         tx = session.beginTransaction();
	         List jobs = session.createQuery("FROM Job").list(); 
	         for (Iterator iterator = jobs.iterator(); iterator.hasNext();){
	            Job job = (Job) iterator.next(); 
	            System.out.print("Title: " + job.getTitle()); 
	            System.out.print("  Skills: " + job.getSkills()); 
	            System.out.println("  Posted Date: " + job.getPostedDate()); 
	         }
	         tx.commit();
	       
	         return jobs;
	      } catch (HibernateException e) {
	         if (tx!=null) tx.rollback();
	         e.printStackTrace(); 
	         return null;
	      } finally {
	         session.close(); 
	      }
		
		
		
	}

	public List<Object> readAll(Request request, Response response)
	{
		//TODO: Your 'GET collection' logic here...
		return Collections.emptyList();
	}

	public void update(Request request, Response response)
	{
		//TODO: Your 'PUT' logic here...
		response.setResponseNoContent();
	}

	public void delete(Request request, Response response)
	{
		//TODO: Your 'DELETE' logic here...
		response.setResponseNoContent();
	}
}
