package com.risksense.assessment.job;
import org.apache.kafka.clients.consumer.*;

import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.common.serialization.LongDeserializer;
import org.apache.kafka.common.serialization.StringDeserializer;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.Collections;
import java.util.Properties;
import org.hibernate.HibernateException; 
import org.hibernate.Session; 
import org.hibernate.Transaction;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class KafkaConsumerExample {
	private final static String TOPIC = "my-example-topic";
    private final static String BOOTSTRAP_SERVERS =
            "localhost:9092,localhost:9093,localhost:9094";
    private static SessionFactory factory; 
    
    public static void main(String... args) throws Exception {
        runConsumer(false);
    }
    
	private static Consumer<Long, String> createConsumer() {
	      final Properties props = new Properties();
	      props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG,
	                                  BOOTSTRAP_SERVERS);
	      props.put(ConsumerConfig.GROUP_ID_CONFIG,
	                                  "KafkaExampleConsumer");
	      props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG,
	              LongDeserializer.class.getName());
	      props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG,
	              StringDeserializer.class.getName());

	      // Create the consumer using props.
	      final Consumer<Long, String> consumer =
	                                  new KafkaConsumer<>(props);

	      // Subscribe to the topic.
	      consumer.subscribe(Collections.singletonList(TOPIC));
	      return consumer;
	  }
	
	 public static void runConsumer(boolean test) throws InterruptedException {
        final Consumer<Long, String> consumer = createConsumer();

        final int giveUp = 3;   int noRecordsCount = 0;
        long time = System.currentTimeMillis();
        try {
	         factory = new Configuration().configure().buildSessionFactory();
	    } catch (Throwable ex) { 
	    	  //ex.printStackTrace();
	         System.err.println("Failed to create sessionFactory object." + ex);
	         throw new ExceptionInInitializerError(ex); 
	    }
        
        Session session = factory.openSession();
	    Transaction tx = null;
        
        while (true) {
            final ConsumerRecords<Long, String> consumerRecords =
                    consumer.poll(5000);
            
            if(test == true) {
	            if (consumerRecords.count()==0) {
	                noRecordsCount++;
	                if (noRecordsCount > giveUp) break;
	                else continue;
	            }
            }
        	      
	        	      try {
	        	    	 tx = session.beginTransaction();
	        	         
	        	         consumerRecords.forEach(record -> {
	        	        	 
	        	        	 ObjectMapper mapper = new ObjectMapper();
	        	        	 
	        	        	
							try {
								Integer jobID = null;
								Job job = mapper.readValue(record.value(), Job.class);
								jobID = (Integer) session.save(job); 
								
							} catch (IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							
							
		        	         
		        	         System.out.printf("%s",record.value());
	        	   //      Job job = new Job();
		        	         
		        	         
	        	         });
	        	      } catch (HibernateException e) {
	        	         if (tx!=null) tx.rollback();
	        	         e.printStackTrace(); 
	        	      } 
	        
            consumer.commitAsync();
        }
        tx.commit(); //commit the changes to db
        session.close(); //close transaction
        consumer.close(); //terminate/close the consummer
        System.out.printf("\nconsumer finished posting in %f seconds \n", (System.currentTimeMillis() - time)/1000.0);
        System.out.println("DONE");
    }
}
