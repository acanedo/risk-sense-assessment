package com.risksense.assessment;

import org.restexpress.util.Environment;

import com.risksense.assessment.job.KafkaConsumerExample;


public class Main
{
	public static void main(String[] args) throws Exception
	{
		System.out.print("Hello World"); 
		
		Configuration config = Environment.load(args, Configuration.class);
		Server server = new Server(config);
		server.start().awaitShutdown();
	}
}
