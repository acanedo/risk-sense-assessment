package com.risksense.assessment;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import com.risksense.assessment.job.KafkaConsumerExample;


public class TestMain {
	private static final String USER_AGENT = "Mozilla/5.0";
	private static final String POST_URL = "http://localhost:8081/test/jobs.json";
	private static final String POST_PARAMS = "{\"title\":\"Software Engineer\",\"skills\":\"math\",\"availability\":\"part-time\",\"country\":\"USA\",\"languages\":\"English\",\"posted_date\":\"06012018\",\"job_type\":\"programmer\",\"experience_level\":\"6 years\",\"pay_rate_max\":200,\"pay_rate_min\":51}";
	
    public static void main(String... args) throws Exception {
    	int n = 1, m = 1; //number of jobs and number of consumers
    	
   
        runJobs(n,m);

    		
    }
	static void runJobs(int nJobs, int nConsumers) throws Exception {
	    	//this method will run n times post request to the restExpress API 
		long time = System.currentTimeMillis();
		
		for(int i = 0; i < nJobs ;i++) {
			System.out.printf("Job number %d", i);
			sendPOST();
		}
		
		System.out.printf("Finished sending %d jobs to kafka queu\n", nJobs);
		System.out.printf("Took about %f seconds to send producers\n", ((System.currentTimeMillis() - time)/1000.0));
		System.out.printf("Now about to process using %d consumers", nConsumers);
		
		// start timer to run consumers
		time = System.currentTimeMillis();
		for(int i = 0; i < nConsumers ;i++) {
			if( nConsumers > 0) {
				if (nConsumers == 1) {
					//always run one consumer to time how long it takes to post to mysql
					KafkaConsumerExample.runConsumer(true);
				}
				else {
					//if more than 1 consumer then the other consumers shall run in "parallel"
					// meaning the need to run as threats or async
					new Thread(() -> {
			            //Do whatever
			        	try {
							KafkaConsumerExample.runConsumer(true);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
			        	
			        }).start();

				}
				
			}
			else{
	                System.out.println("Consumers must be greater than 0");
			}
		}
		
		System.out.printf("Finished consuming %d jobs using %d kafka consumers\n", nJobs, nConsumers);
		System.out.printf("Took about %f seconds to post to database", ((System.currentTimeMillis() - time)/1000.0));
		
	}
	
	private static void sendPOST() throws IOException {
		URL obj = new URL(POST_URL);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
		con.setRequestMethod("POST");
		con.setRequestProperty("User-Agent", USER_AGENT);

		// For POST only - START
		con.setDoOutput(true);
		OutputStream os = con.getOutputStream();
		os.write(POST_PARAMS.getBytes());
		os.flush();
		os.close();
		// For POST only - END

		int responseCode = con.getResponseCode();
		//System.out.println("POST Response Code :: " + responseCode);

		if (responseCode == HttpURLConnection.HTTP_OK) { //success
			BufferedReader in = new BufferedReader(new InputStreamReader(
					con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();

			// print result
			//System.out.println(response.toString());
		} else {
			System.out.println("POST request didnt work");
		}
	}
    
}

	